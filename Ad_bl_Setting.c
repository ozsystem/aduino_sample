#include <SoftwareSerial.h>
#define	BT_TX	7
#define	BT_RX	6

SoftwareSerial BTSerial(BT_TX, BT_RX);

void setup()
{
	Serial.begin(9600);
	BTSerial.begin(9600);
}

void loop()
{
	if (BTSerial.available())
	{
		Serial.write("[");
		Serial.write(BTSerial.read());
		Serial.write("]\n");
	}

	if (Serial.available())
	{
		BTSerial.write(Serial.read());
	}
}


//1. 'AT' 입력 후 전송
//2. 'OK' 확인
//3. 'AT+NAME원하는이름' 입력 후 전송
//4. 'OKsetname' 확인
//5. 'AT+PIN원하는숫자'
//6. 'OKsetPIN' 확인

